# Escape Room VRGameJam

**Hi There Your Office Cat here, Etow! Presenting you a little game I created during a VR GameJam at the GPB**

![](https://media.giphy.com/media/jUiko9b4A2rfNAB3eh/giphy.gif)

Little VR game I created during a GameJam at the GPB Together with a fun group of artist :smiley: ! We decided to create this escape room game, or rather "steal room". Goal of the game is to steal an artifact hidden in the room within a given time, to do so the player has to find clues and solve puzzles around the room. All in all it was a very fun project to work on! Mostly for VR game interactions since puzzle/escape rooms require a lot of interaction with the environment.

**GameBuild Dowload**

https://www.dropbox.com/s/sbeqrro0y0ajouq/StealRoom.zip?dl=0

**Preview of the Game:**

https://youtu.be/N01lnmvlOkI

**Pictures of the Game:**

![Alt Text](https://i.imgur.com/PfEh4P2.png)


**Programming the Game:**

Programming this game was a new challenge for me since I had to create all kinds of player/environment interactions for VR. Admittably at first I encoutered some problems, being confronted for the first time with Unity Tools like joints and creating doors/buttons. But I managed to make all of them work and now fully learned how to easily create theses kinds of interactions in a VR game, which will be very usefull to me in some of my other projects like [Reactor Engineer](https://gitlab.com/EtowTheOfficeCat/project-108_vr-reactor-engineer).
Since this was a GameJame the programming is sadly not the cleanest! :D but in the time I had I did manage to make everything work well. 

**Still to fix:**

- Unfortunatly I encoutered one bug in the Gamebuild which doesnt allow the player to use the endgame menu.